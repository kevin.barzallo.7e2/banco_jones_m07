<?php
require_once('../Helpers/DBManager.php');

class init_model{

    private $manager;

    public function __construct(){
        $this->manager=new DBManager();
    }

    public function getCuentas($id){
        try{
            $sql = "SELECT id FROM cuenta WHERE id_cliente = :id";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':id',$id);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (empty(array_filter($result))){
                $this->setNuevaCuenta($id);
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                return $result;
            }else{
                return $result;
            }
        }catch (PDOException $e){ echo $e->getMessage(); }

    }

    public function getSaldo($id){
        try{
            $sql = "SELECT saldo FROM cuenta WHERE id = :id";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':id',$id);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result[0]['saldo'];
        }catch (PDOException $e){ echo $e->getMessage(); }

    }

    public function setNuevaCuenta($id){

        try{
            $sql = "INSERT INTO cuenta (id_cliente,saldo) VALUES (:id,0)";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':id',$id);
            $stmt->execute();

        }catch (PDOException $e){ echo $e->getMessage(); }

    }

    public function getMovimientos($cuenta){
        try {
            $sql = "SELECT * FROM movimientos WHERE id_origen=:cuenta or id_destino=:cuenta";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':cuenta', $cuenta);
            $stmt->execute();
            $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $this->manager->closeConexion();
            return $rt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

}

?>