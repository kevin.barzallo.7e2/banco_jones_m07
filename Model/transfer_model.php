<?php

require_once('../Helpers/DBManager.php');

class transfer_model{

    private $manager;

    public function __construct(){
        $this->manager=new DBManager();
    }

    public function transfer($iban_origen,$iban_destino,$cantidad,$comentario){

        try{
            $sql = "INSERT INTO movimientos (cantidad,id_origen,id_destino,comentario,fecha) VALUES (:cantidad,:iban_origen,:iban_destino,:comentario,now())";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':cantidad',$cantidad);
            $stmt->bindParam(':iban_destino',$iban_destino);
            $stmt->bindParam(':iban_origen',$iban_origen);
            $stmt->bindParam(':comentario',$comentario);
            if ($stmt->execute()){
                $sql = "UPDATE cuenta SET saldo=saldo+:cantidad WHERE id=:iban_destino";
                $stmt = $this->manager->getConexion()->prepare($sql);
                $stmt->bindParam(':cantidad',$cantidad);
                $stmt->bindParam(':iban_destino',$iban_destino);
                if ($stmt->execute()){
                    return true;
                }else { return false;}
            }else{
                return false;
            }

        }catch (PDOException $e){ echo $e->getMessage(); }

    }
}

?>
