<?php

require_once('../Helpers/DBManager.php');

class profile_model{

    private $manager;

    public function __construct(){
        $this->manager=new DBManager();
    }

    public function getInfo($id){
        try {
            $sql = "SELECT * FROM cliente WHERE id =:id";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':id',$id);
            if ($stmt->execute()){
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                return $result;
            }else{ return "EROOOOORRR";}

        }catch (PDOException $e){
            echo $e->getMessage();
        }
        return "Error";
    }

    public function setModifyWPass($id,$numtel,$email,$password){
        try {
            $sql = "UPDATE cliente SET telefono=:numTel, email=:email, password=:password WHERE id=:id";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':id',$id);
            $stmt->bindParam(':email',$email);
            $stmt->bindParam(':password',$password);
            $stmt->bindParam(':numTel',$numtel);
            $stmt->execute();
        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    public function setModifyWOPass($id,$numtel,$email){
        try {
            $sql = "UPDATE cliente SET telefono=:numTel, email=:email WHERE id=:id";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':id',$id);
            $stmt->bindParam(':email',$email);
            $stmt->bindParam(':numTel',$numtel);
            $stmt->execute();
        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    public function updateImage($imagen,$id){
        try {
            $sql = "UPDATE cliente SET image =:imagen WHERE id =:id";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':id',$id);
            $stmt->bindParam(':imagen',$imagen,PDO::PARAM_LOB);
            $stmt->execute();

        }catch (PDOException $e){
            echo $e->getMessage();

        }
    }

}

?>