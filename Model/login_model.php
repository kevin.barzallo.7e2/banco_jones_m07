<?php
require_once('../Helpers/DBManager.php');

class login_model{

    private $manager;

    public function __construct(){
        $this->manager=new DBManager();
    }

    public function getUserHash($username){
        try{
            $sql = "SELECT * FROM cliente WHERE dni=:username";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':username',$username);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result[0]['password'];
        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    public function getName($dni){
        try{
            $sql = "SELECT nombre FROM cliente WHERE dni=:dni";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':dni',$dni);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result[0]['nombre'];
        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    public function getId($dni){
        try {
            $sql = "SELECT id FROM cliente WHERE dni=:dni";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':dni',$dni);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result[0]['id'];
        }catch (PDOException  $e){
            echo $e->getMessage();
        }

    }

}

?>
