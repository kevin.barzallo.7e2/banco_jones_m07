<?php
require_once('../Helpers/DBManager.php');
require_once('../Model/Cliente.php');

class register_model{

    private $manager;

    public function __construct(){
        $this->manager=new DBManager();
    }

    function insertCliente($cliente){

        try{
            $sql="INSERT INTO cliente (nombre, apellidos, nacimiento, sexo, telefono, dni, email, password) 
              VALUES (:nombre,:apellidos,:nacimiento,:sexo,:telefono,:dni,:email,:password)";

            $nombre=$cliente->getNombre();
            $nacimiento=$cliente->getNacimiento();
            $apellidos=$cliente->getApellidos();
            $sexo=$cliente->getSexo();
            $email=$cliente->getEmail();
            $telefono=$cliente->getTelefono();
            $dni=$cliente->getDni();
            $password=password_hash($cliente->getPassword(),PASSWORD_DEFAULT,['cost'=> 10]);

            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':nombre',$nombre);
            $stmt->bindParam('nacimiento',$nacimiento);
            $stmt->bindParam(':apellidos',$apellidos);
            $stmt->bindParam(':sexo',$sexo);
            $stmt->bindParam(':email',$email);
            $stmt->bindParam(':telefono',$telefono);
            $stmt->bindParam(':dni',$dni);
            $stmt->bindParam(':password',$password);


            if ($stmt->execute()){
                return true;
            }else{ return false; }

        }catch (PDOException $e){ echo $e->getMessage(); }
    }
    function setNuevaCuenta($dni){
        try {
            $sql = "SELECT id FROM cliente WHERE dni = :dni";
            $stmt = $this->manager->getConexion()->prepare($sql);
            $stmt->bindParam(':dni',$dni);

            if ($stmt->execute()){
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $id = $result[0]['id'];
                $sql = "INSERT INTO cuenta (id_cliente,saldo) VALUES (:id,0)";
                $stmt = $this->manager->getConexion()->prepare($sql);
                $stmt->bindParam(':id',$id);
                if ($stmt->execute()){
                    return  true;
                }else{ print_r($stmt->errorInfo(), false); }
            }else{ print_r($stmt->errorInfo(), false); }

        }catch (PDOException $e){ echo $e->getMessage(); }
    }

}


?>
