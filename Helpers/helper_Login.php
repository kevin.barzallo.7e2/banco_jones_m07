<?php

//////////////////////////////////////////////////////////////////
/// ================== FUNCIONES ==================
//////////////////////////////////////////////////////////////////

function campoDNI($dni){
    $numeros= substr($dni, 0, -1);
    $lletra= substr($dni, -1);
    $modulDNI= $numeros % 23;
    $taulaLletres= "TRWAGMYFPDXBNJZSQVHLCKE";
    $validaLletra= substr($taulaLletres, $modulDNI, 1);
    if ($validaLletra!=$lletra) {
        return false;
    } else {
        return true;
    }
}

function campoPassword($passwd){
    if (preg_match('/(?=.*\d)(?=.+\W)(?=.*[a-z])(?=.*[A-Z]).{8,}/', $passwd)){
        return true;
    }else{
        return false;
    }
}

//////////////////////////////////////////////////////////////////
/// ================== VARIABLES ==================
//////////////////////////////////////////////////////////////////

$error=false;
$missatgerror="";


//////////////////////////////////////////////////////////////////
/// ================== FORM ACTION ==================
//////////////////////////////////////////////////////////////////

if (isset($_POST['login'])){

    if (!campoDNI($_POST['dni'])){
        $missatgerror= "El usuario o contraseña no son correctos";
        $error = true;
    }

    if (!campoPassword($_POST['passwd'])){
        $missatgerror= "El usuario o contraseña no son correctos";
        $error = true;
    }

} elseif (isset($_POST['register'])){
    header('Location: ../Views/register.php');
}


?>
