<?php

//////////////////////////////////////////////////////////////////
/// ================== FUNCIONES ==================
//////////////////////////////////////////////////////////////////

function campoNombreApellidos($valor){
    $sinEspacios=str_replace(' ', '', $valor);
    if (trim($valor)=="" or !(ctype_alpha($sinEspacios))){
        return false;
    }
    return true;
}

function campoNacimiento($fecha){
    if ($fecha == null){
        return false;
    }else{
        $splitFecha= explode("-", $fecha);
        if (date('Y') - $splitFecha[0] > 18) { return true; } else {
            if (date('Y') - $splitFecha[0] == 18) {
                if (date('m') - $splitFecha[1] > 0) { return true; } else {
                    if (date('m') - $splitFecha[1] == 0) {
                        if (date('d') - $splitFecha[2] >= 0) { return true; }
                    }
                }
            }
        }
        return false;
    }
}

function campoDNI($dni){
    $numeros= substr($dni, 0, -1);
    $lletra= substr($dni, -1);
    $modulDNI= $numeros % 23;
    $taulaLletres= "TRWAGMYFPDXBNJZSQVHLCKE";
    $validaLletra= substr($taulaLletres, $modulDNI, 1);
    if ($validaLletra!=$lletra) {
        return false;
    } else {
        return true;
    }
}

function campoNumTel($num){
    if (preg_match('/^(6|7)/', $num)){
        if (strlen($num) == 9){
            return true;
        }
        return false;
    }else{
        return false;
    }
}

function campoEmail($email){
    if (preg_match('/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/', $email)){
        return true;
    }else{
        return false;
    }
}

function campoPassword($passwd){
    if (preg_match('/(?=.*\d)(?=.+\W)(?=.*[a-z])(?=.*[A-Z]).{8,}/', $passwd)){
        return true;
    }else{
        return false;
    }
}

//////////////////////////////////////////////////////////////////
/// ================== VARIABLES ==================
//////////////////////////////////////////////////////////////////

$error=false;
$missatgerror['campoNombre']="";
$missatgerror['campoApellidos']="";
$missatgerror['campoSexo']="";
$missatgerror['campoNacimiento']="";
$missatgerror['campoDNI']="";
$missatgerror['campoNumTel']="";
$missatgerror['campoEmail']="";
$missatgerror['campoPassword']="";

$guarda_valors['campoNombre']="";
$guarda_valors['campoApellidos']="";
$guarda_valors['campoSexo']="";
$guarda_valors['campoNacimiento']="";
$guarda_valors['campoDNI']="";
$guarda_valors['campoNumTel']="";
$guarda_valors['campoEmail']="";
$guarda_valors['campoPassword']="";

//////////////////////////////////////////////////////////////////
/// ================== FORM ACTION ==================
//////////////////////////////////////////////////////////////////

if (isset($_REQUEST['registerButton'])){

    if (campoNombreApellidos($_REQUEST['campoNombre'])){
        $guarda_valors['campoNombre'] = $_REQUEST['campoNombre'];
    }else{
        $missatgerror['campoNombre'] = "Sólo pueden aparecer letras y espacios";
        $error = true;
    }

    if (campoNombreApellidos($_REQUEST['campoApellidos'])){
        $guarda_valors['campoApellidos'] = $_REQUEST['campoApellidos'];
    }else{
        $missatgerror['campoApellidos'] = "Sólo pueden aparecer letras y espacios";
        $error = true;
    }

    if ($_REQUEST['campoSexo'] != null){
        $guarda_valors['campoSexo'] = $_REQUEST['campoSexo'];
    }else{
        $missatgerror['campoSexo'] = "Elige uno!";
        $error = true;
    }

    if (campoNacimiento($_REQUEST['campoNacimiento'])){
        $guarda_valors['campoNacimiento'] = $_REQUEST['campoNacimiento'];
    }else{
        $missatgerror['campoNacimiento'] = "No eres mayor de 18 años!";
        $error = true;
    }

    if (campoDNI($_REQUEST['campoDNI'])){
        $guarda_valors['campoDNI'] = $_REQUEST['campoDNI'];
    }else{
        $missatgerror['campoDNI'] = "El DNI no es válido!";
        $error = true;
    }

    if (campoNumTel($_REQUEST['campoNumTel'])){
        $guarda_valors['campoNumTel'] = $_REQUEST['campoNumTel'];
    }else{
        $missatgerror['campoNumTel'] = "9 cifras empezando por 6 o 7";
        $error = true;
    }

    if (campoEmail($_REQUEST['campoEmail'])){
        $guarda_valors['campoEmail'] = $_REQUEST['campoEmail'];
    }else{
        $missatgerror['campoEmail'] = "Email no valido!";
        $error = true;
    }

    if (campoPassword($_REQUEST['campoPassword'])){
        if (strcmp($_REQUEST['campoPassword'],$_REQUEST['campoVerificaPasswd']) !== 0){
            $missatgerror['campoPassword'] = "No coincide las contraseñas!";
            $error = true;
        }else{ $guarda_valors['campoPassword'] = $_REQUEST['campoPassword']; }
    }else{
        $missatgerror['campoPassword'] = "Que contenga mayúsculas, minúsculas, números, un carácter especial y más de 8 caracteres";
        $error = true;
    }

}elseif (isset($_POST['atrasButton'])){
    header('Location: ../Views/login.php');
}


?>