<?php

session_start();
$_POST['id'] = $_SESSION['id'];
$_POST['iban_origen'] = $_SESSION['iban_origen'];
session_write_close();

//////////////////////////////////////////////////////////////////
/// ================== FUNCIONES ==================
//////////////////////////////////////////////////////////////////

function campoDestino($iban){
    if (preg_match('/[0-9]+/',$iban)){
        return true;
    }else{
        return false;
    }
}

function campoCantidad($cantidad){
    if (preg_match('/^\d+((,|.)\d{1,2})?$/', $cantidad)){
        return true;
    }else{
        return false;
    }
}

function campoComentario($comentario){
    if (ctype_alnum($comentario)){
        return true;
    }else{
        return false;
    }
}

//////////////////////////////////////////////////////////////////
/// ================== VARIABLES ==================
//////////////////////////////////////////////////////////////////

$error=false;
$comentario=false;
$missatgerror['iban_destino']="";
$missatgerror['cantidad_transfer']="";
$missatgerror['comentario_transfer']="";

$guarda_valors['iban_destino']="";
$guarda_valors['cantidad_transfer']="";
$guarda_valors['comentario_transfer']="";

//////////////////////////////////////////////////////////////////
/// ================== FORM ACTION ==================
//////////////////////////////////////////////////////////////////


if (isset($_REQUEST['transferencia'])){

    if (campoDestino($_REQUEST['iban_destino'])){
        $guarda_valors['iban_destino'] = $_REQUEST['iban_destino'];
    }else{
        $missatgerror['iban_destino'] = "Solo debe contener numeros";
        $error = true;
    }

    if (campoCantidad($_REQUEST['cantidad_transfer'])){
        $guarda_valors['cantidad_transfer'] = $_REQUEST['cantidad_transfer'];
    }else{
        $missatgerror['cantidad_transfer'] = "Solo debe contener numeros";
        $error = true;
    }

    if (campoComentario($_REQUEST['comentario_transfer'])){
        $guarda_valors['comentario_transfer'] = $_REQUEST['comentario_transfer'];
    }else{
        $missatgerror['comentario_transfer'] = "Solo debe contener letras y numeros";
        $error = true;
    }

}


?>
