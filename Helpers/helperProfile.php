<?php


session_start();
$_POST['id'] = $_SESSION['id'];
session_write_close();

//////////////////////////////////////////////////////////////////
/// ================== FUNCIONES ==================
//////////////////////////////////////////////////////////////////

function campoNumTel($num){
    if (preg_match('/^(6|7)/', $num)){
        if (strlen($num) == 9){
            return true;
        }
        return false;
    }else{
        return false;
    }
}

function campoEmail($email){
    if (preg_match('/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/', $email)){
        return true;
    }else{
        return false;
    }
}

function campoPassword($passwd){
    if (preg_match('/(?=.*\d)(?=.+\W)(?=.*[a-z])(?=.*[A-Z]).{8,}/', $passwd)){
        return true;
    }else{
        return false;
    }
}

//////////////////////////////////////////////////////////////////
/// ================== VARIABLES ==================
//////////////////////////////////////////////////////////////////

$error=false;
$changePass=false;
$missatgerror['campoNumTel']="";
$missatgerror['campoEmail']="";
$missatgerror['campoPassword']="";

$guarda_valors['campoNumTel']="";
$guarda_valors['campoEmail']="";
$guarda_valors['campoPassword']="";

//////////////////////////////////////////////////////////////////
/// ================== FORM ACTION ==================
//////////////////////////////////////////////////////////////////

if (isset($_REQUEST['update'])){

    if (campoNumTel($_REQUEST['campoNumTel'])){
        $guarda_valors['campoNumTel'] = $_REQUEST['campoNumTel'];
    }else{
        $missatgerror['campoNumTel'] = "9 cifras empezando por 6 o 7";
        $error = true;
    }

    if (campoEmail($_REQUEST['campoEmail'])){
        $guarda_valors['campoEmail'] = $_REQUEST['campoEmail'];
    }else{
        $missatgerror['campoEmail'] = "Email no valido!";
        $error = true;
    }

    if ($_REQUEST['campoPassword'] != null){
        if (campoPassword($_REQUEST['campoPassword'])){
            $guarda_valors['campoPassword'] = $_REQUEST['campoPassword'];
            $changePass=true;
        }else{
            $missatgerror['campoPassword'] = "Que contenga mayúsculas, minúsculas, números, un carácter especial y más de 8 caracteres";
            $error = true;
        }
    }
}else if (isset($_REQUEST['back'])){
    header('Location: ../Views/init.php');

}


?>
