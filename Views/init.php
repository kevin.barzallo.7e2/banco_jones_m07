<!DOCTYPE html>
<html>
<head>
    <title>Banco Jones - Inicio</title>
    <meta charset='utf-8'>
    <link rel='stylesheet' href='../CSS/init.css'>
    <style>

    </style>
</head>
<body>
    <header>
        <?php
        session_start();
        if (isset($_SESSION['nombre'])){
            echo "<h3>Hola de nuevo ".$_SESSION['nombre']."</h3>";
            require_once("../Model/init_model.php");

            $conexion = new init_model();
            $arrayCuentas = $conexion->getCuentas($_SESSION['id']);


        ?>
        <nav>
            <a href="../Views/profile.php">Perfil</a>
            <a href="../Views/logout.php">Desconectar</a>
        </nav>
    </header>
    <form action="../Controller/initController.php" method="post">
        <table>
            <tr>
                <td>CUENTAS</td>
                <td><input type="submit" name="nuevaCuenta" value="Nueva cuenta"></td>
                <td><?php echo $_POST['message'] ?></td>
            </tr>
            <?php

            foreach ($arrayCuentas as $index){
                $idk = $index;
                echo "<tr><td>IBAN <input class='buttono' type='submit' name='iban' value='".$index['id']."'></td></tr>";
            }
            ?>
        </table>
    </form>

<?php
        session_write_close();
        } else {
            echo "<h3>Tu sesion ha expirado, largo de aqui.</h3>";
            header("refresh:3;url=../Views/login.php");
        }
        ?>


</body>
</html>
