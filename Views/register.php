<?php
require_once('../Helpers/i18n.php')
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo _("Registro");?></title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
        *{
            font-family: "Roboto", serif;
        }
        .error {
            color: red;
        }
        fieldset {
            border: 5px solid cyan;
            padding:16px;
        }
        legend {
            text-align: center;
        }

        table {
            margin: auto;
        }

        td {
            padding: 5px;
        }

    </style>
</head>
<body>
<?php require_once('header.php')?>

<form action="../Controller/register_Controller.php" method="post">
    <fieldset>
        <legend>REGISTRO</legend>
        <table>
            <tr>
                <td>Nombre: </td>
            <td><input type="text" name="campoNombre" value="<?php if ($guarda_valors['campoNombre']!="") echo $guarda_valors['campoNombre'] ?>"></td>
                <td>
                    <?php
                    if ($missatgerror['campoNombre'] !=""){
                        echo '<span class="error">' . $missatgerror['campoNombre'] . '</span>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td>Apellidos: </td>
                <td><input type="text" name="campoApellidos" value="<?php if ($guarda_valors['campoApellidos']!="") echo $guarda_valors['campoApellidos'] ?>"></td>
                <td>
                    <?php
                    if ($missatgerror['campoApellidos'] !=""){
                        echo '<span class="error">' . $missatgerror['campoApellidos'] . '</span>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td>Sexo</td>
                <td>
                    <input type="radio" name="campoSexo" value="M" <?php if ($guarda_valors['campoSexo'] == "M") echo 'checked'?>><label> Mujer </label>
                    <input type="radio" name="campoSexo" value="H" <?php if ($guarda_valors['campoSexo'] == "H") echo 'checked'?>><label> Hombre </label>
                </td>
                <td>
                    <?php
                    if ($missatgerror['campoSexo'] !=""){
                        echo '<span class="error">' . $missatgerror['campoSexo'] . '</span>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td>Fecha de nacimiento: </td>
                <td><input type="date" name="campoNacimiento" value="<?php if ($guarda_valors['campoNacimiento']!="") echo $guarda_valors['campoNacimiento'] ?>"></td>
                <td>
                    <?php
                    if ($missatgerror['campoNacimiento'] !=""){
                        echo '<span class="error">' . $missatgerror['campoNacimiento'] . '</span>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td>DNI: </td>
                <td><input type="text" name="campoDNI" value="<?php if ($guarda_valors['campoDNI']!="") echo $guarda_valors['campoDNI'] ?>"></td>
                <td>
                    <?php
                    if ($missatgerror['campoDNI'] !=""){
                        echo '<span class="error">' . $missatgerror['campoDNI'] . '</span>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td>Número de teléfono: </td>
                <td><input type="text" name="campoNumTel" value="<?php if ($guarda_valors['campoNumTel']!="") echo $guarda_valors['campoNumTel'] ?>"></td>
                <td>
                    <?php
                    if ($missatgerror['campoNumTel'] !=""){
                        echo '<span class="error">' . $missatgerror['campoNumTel'] . '</span>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td>Email: </td>
                <td><input type="text" name="campoEmail" value="<?php if ($guarda_valors['campoEmail']!="") echo $guarda_valors['campoEmail'] ?>"></td>
                <td>
                    <?php
                    if ($missatgerror['campoEmail'] !=""){
                        echo '<span class="error">' . $missatgerror['campoEmail'] . '</span>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td>Password: </td>
                <td><input type="password" name="campoPassword" value="<?php if ($guarda_valors['campoPassword']!="") echo $guarda_valors['campoPassword'] ?>"></td>
                <td>
                    <?php
                    if ($missatgerror['campoPassword'] !=""){
                        echo '<span class="error">' . $missatgerror['campoPassword'] . '</span>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td></td>
                <td><input type="password" name="campoVerificaPasswd" placeholder="Repite el password"></td>
            </tr>

            <tr>
                <td></td>
                <td><input type="submit" name="atrasButton" value="Voler a Login"></td>
            </tr>

            <tr>
                <td></td>
                <td><input type="submit" name="registerButton" value="Darse de alta"></td>
            </tr>


        </table>
    </fieldset>

</form>

</body>
</html>