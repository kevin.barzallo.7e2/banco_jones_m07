
<html>
<head>
    <meta charset="utf-8">
    <title>Logout</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
        *{
            font-family: "Roboto", serif;
        }
        h3{
            margin-top: 10%;
            text-align: center;
        }
    </style>
</head>
<body>
<?php
session_start();
echo "<h3>Hasta luego ".$_SESSION['nombre']."</h3>";
session_destroy();
//header('Location: login.php');
header("refresh:3;url=login.php");

?>
</body>
</html>


