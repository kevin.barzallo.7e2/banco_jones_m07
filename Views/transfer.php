<!DOCTYPE html>
<html>
<head>
    <title>Banco Jones - Transfer</title>
    <link rel='stylesheet' href='../CSS/login.css'>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
<?php
session_start();
if (isset($_SESSION['nombre'])){

?>

    <form action="../Controller/transferController.php" method="post">
        <fieldset>
            <table>
                <legend>TRANSFERENCIA</legend>
                <tr>
                    <td>Destino: </td>
                    <td><input type="text" name="iban_destino" value="<?php if ($guarda_valors['iban_destino']!="") echo $guarda_valors['iban_destino'] ?>"></td>
                    <td>
                        <?php
                        if ($missatgerror['iban_destino'] !=""){
                            echo '<span class="error">' . $missatgerror['iban_destino'] . '</span>';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Cantidad: </td>
                    <td><input type="text" name="cantidad_transfer" value="<?php if ($guarda_valors['cantidad_transfer']!="") echo $guarda_valors['cantidad_transfer'] ?>"></td>
                    <td>
                        <?php
                        if ($missatgerror['cantidad_transfer'] !=""){
                            echo '<span class="error">' . $missatgerror['cantidad_transfer'] . '</span>';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Comentario: </td>
                    <td><input type="text" name="comentario_transfer" value="<?php if ($guarda_valors['comentario_transfer']!="") echo $guarda_valors['comentario_transfer'] ?>"></td>
                    <td>
                        <?php
                        if ($missatgerror['comentario_transfer'] !=""){
                            echo '<span class="error">' . $missatgerror['comentario_transfer'] . '</span>';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="transferencia" value="Hacer transferencia"></td>
                    <td><a href="../Views/init.php"> Volver </a></td>

                </tr>

            </table>
        </fieldset>
    </form>
    <?php
}else{
    echo "<h3>Tu sesion ha expirado, largo de aqui.</h3>";
    header("refresh:3;url=../Views/login.php");
}
?>
</body>
</html>
