<!DOCTYPE html>
<html>
<head>
    <title>Banco Jones - Login</title>
    <meta charset='utf-8'>
    <link rel='stylesheet' href='../CSS/login.css'>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>

    <form action="../Controller/login_Controller.php" method="post">
        <fieldset>
            <table>
                <legend>LOGIN</legend>
                <tr>
                    <td>Usuario: </td>
                    <td><input type="text" name="dni" ></td>
                </tr>
                <tr>
                    <td>Contraseña: </td>
                    <td><input type="password" name="passwd" ></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="login" value="Entrar">
                        <input type="submit" name="register" value="Registrarse">
                        <?php
                        if ($missatgerror !=""){
                            echo '<span class="error">' . $missatgerror . '</span>';
                        }
                        ?>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>

</body>
</html>
