<script>
    function changeLang(){
        document.getElementById('form_lang').submit();
    }
</script>


<form method='get' action='../Controller/lang_Controller.php' >
    Select Language : <select name='lang' onchange='this.form.submit()' >
        <option value='es' <?php if(isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'es'){ echo "selected"; } ?> >Castellano</option>
        <option value='ca' <?php if(isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'ca'){ echo "selected"; } ?> >Català</option>
    </select>
    <input type="hidden" name="switch_lang" value="switch_lang">
</form>