<!DOCTYPE html>
<html>
<head>
    <title>Ultimos movimientos</title>
</head>
<body>
<?php
session_start();
if (isset($_SESSION['nombre'])){
    if (isset($_SESSION['saldo'])) {
        echo "Saldo " . $_SESSION['saldo'] . '<br/>';
        require_once('../Model/init_model.php');
        $conexion = new init_model();
        $accounts=$conexion->getCuentas($_SESSION['id']);
    }
    if (isset($_SESSION['lista'])) {
        $movimientos=$_SESSION['lista'];
        echo '<table class="default" rules="all" frame="border">';
        echo '<tr>';
        echo '<th>origen</th>';
        echo '<th>destino</th>';
        echo '<th>hora</th>';
        echo '<th>cantidad</th>';
        echo '</tr>';
        for ($i=0;$i<count($movimientos);$i++){
            echo '<tr>';
            echo '<td>'.$movimientos[$i]['id_origen'].'</td>';
            echo '<td>'.$movimientos[$i]['id_destino'].'</td>';
            echo '<td>'.$movimientos[$i]['fecha'].'</td>';
            echo '<td>'.$movimientos[$i]['cantidad'].'</td>';
            echo '</tr>';
        }
        echo '</table>';

    }

?>
<form action="../Controller/query_Controller.php" method="post">
    <select name="cuenta">

        <?php
        for ($i=0; $i<sizeof($accounts) ;$i++){?>
            <option ><?php echo $accounts[$i]["id"] ?></option>
        <?php }?>
    </select>
    <input name="submit" type="submit" value="Seleccionar"/>
    <input name="control" type="hidden" value="query"/>
    <input name="back" type="submit" value="Volver al inicio">
</form>

<?php
    }else{
    echo "<h3>Tu sesion ha expirado, largo de aqui.</h3>";
    header("refresh:3;url=login.php");
}

?>

</body>
</html>

