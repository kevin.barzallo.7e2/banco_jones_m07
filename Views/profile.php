<!DOCTYPE html>
<html>
<head>
    <title>Banco Jones - Perfil</title>
    <link rel='stylesheet' href='../CSS/login.css'>
    <style>
        table{
            border-collapse:separate;
            border-spacing: .5em 1em;
        }
    </style>
</head>
<body>

<?php
require_once("../Helpers/helperProfile.php");

if (isset($_SESSION['nombre'])){
    require_once("../Model/profile_model.php");

    $infoCliente = new profile_model();
    $campo = $infoCliente->getInfo($_POST['id']);
    ob_start();
    fpassthru($campo[0]['image']);
    $data = ob_get_contents();
    ob_end_clean();
    $img = "data:image/*;base64,".base64_encode($data);


    ?>

<form action="../Controller/profileController.php" method="post" enctype="multipart/form-data">

    <fieldset>
        <legend>Información</legend>
        <table>
            <tr>
                <td>Nombre: </td>
                <td><input type="text" value="<?php echo $campo[0]['nombre'] ?>" readonly></td>
                <td><?php echo "<img src='".$img."'/>"; ?></td>
            </tr>

            <tr>
                <td>Apellidos: </td>
                <td><input type="text" value="<?php echo $campo[0]['apellidos'] ?>" readonly></td>
            </tr>

            <tr>
                <td>Sexo:</td>
                <td>
                    <input type="text" value="<?php echo $campo[0]['sexo'] ?>" readonly></td>
            </tr>

            <tr>
                <td>Fecha de nacimiento: </td>
                <td><input type="date" value="<?php echo $campo[0]['nacimiento'] ?>" readonly></td>
            </tr>

            <tr>
                <td>DNI:</td>
                <td><input type="text" value="<?php echo $campo[0]['dni'] ?>" readonly></td>
            </tr>

            <tr>
                <td>Número de teléfono:</td>
                <td><input type="text" name="campoNumTel" value="<?php echo $campo[0]['telefono'] ?>"></td>
                <td>
                    <?php
                    if ($missatgerror['campoNumTel'] !=""){
                        echo '<span class="error">' . $missatgerror['campoNumTel'] . '</span>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td>Email: </td>
                <td><input type="text" name="campoEmail" value="<?php echo $campo[0]['email'] ?>"></td>
                <td>
                    <?php
                    if ($missatgerror['campoEmail'] !=""){
                        echo '<span class="error">' . $missatgerror['campoEmail'] . '</span>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td>Password: </td>
                <td><input type="password" name="campoPassword" value=""></td>
                <td>
                    <?php
                    if ($missatgerror['campoPassword'] !=""){
                        echo '<span class="error">' . $missatgerror['campoPassword'] . '</span>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td>Foto de perfil: </td>
                <td><input type="file" name="upload" id="upload"></td>
            </tr>

            <tr>
                <td></td>
                <td>
                    <input type="submit" name="update" value="Actualizar">
                    <input type="submit" name="back" value="Volver a init">
                </td>
            </tr>

            <input type="hidden" value="profile" name="control">


        </table>
    </fieldset>

</form>
<?php
}else{
    echo "<h3>Tu sesion ha expirado, largo de aqui.</h3>";
    header("refresh:3;url=../Views/login.php");
}
?>
</body>
</html>