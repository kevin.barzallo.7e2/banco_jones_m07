<!DOCTYPE html>
<html lang=es>
<head>
    <meta name="author" content="Kevin Barzallo">
    <meta name="keywords" content="HTML,PHP">
    <meta name="description" content="Pagina exercicis PHP">
    <meta charset="UTF-8">
    <title>EX25 Cerca Alumne</title>
    <style type="text/css">
        /* General */
        BODY {background-color:lightyellow; font-family: verdana,arial, sans-serif; font-size: 10pt;}

        /* Contenido */
        H1 {font-size: 16pt; font-weight: bold; color: #0066CC;}
        H2 {font-size: 12pt; font-weight: bold; font-style: italic; color: black;}
        H3 {font-size: 10pt; font-weight: bold; color: black;}

        /* Formulario */
        FORM.borde {border: 1px dotted #0066CC; padding: 0.5em 0.2em; width: 80%;}
        FORM p {clear: left; margin: 0.2em; padding: 0.1em;}
        FORM p LABEL {float: left; width: 25%; font-weight: bold;}
        .error {color: red;}
    </style>
</head>
<body>
<?php

////////////////FUNCTIONS///////////////////
//
//   Espai destinat a les funcions
//

//Esta funcion se encarga de validar los campos de texto y que solo sean alfabeticos
function validaCamp($name, $valor){
    $missatge="";
    $sinEspacios=str_replace(' ', '', $valor);;
    if (trim($valor)=="" or !(ctype_alpha($sinEspacios))){

        $missatge="El camp $name és buit o conté digits";

    }
    return $missatge;
}

//funcion que muestra el select del alumno declarado, se pasa el nombre del alumno a buscar
function mostrarSelect($alumne){
    /*----------CONNEXIó AL GESTOR -----*/
    $servidor="localhost";
    $usuario="root";
    $pass="";
    $dbname="academia";
    $taula="Alumnes";
    $conexion = mysqli_connect($servidor,$usuario,$pass, $dbname);
    // Check connection
    if (!$conexion) {
        die("Error al connectar-se a la base de dades $dbname:" . mysqli_connect_error());
    }
    // sentencia sql para mostrar datos de Alumnes solo si Nom es igual al nombre declarado
    $sql = "SELECT * FROM $taula WHERE Nom='$alumne' ";
    if ($result=mysqli_query($conexion, $sql)) {
        // Con esto me dice cuantas filas ha devuelto
        $row_cnt = mysqli_num_rows($result);
        // si ha devuelto 0 significa que no existe nadie llamado con el nombre declarado
        if ($row_cnt==0){
            echo "<br>";
            // enseña error
            echo "<span class='error'>ERROR!No existe este alumno! </span>";
            echo "<br>";

        }else{
            // si esta bien enseña los datos de este alumno
            echo "<br>";
            while($reg=mysqli_fetch_assoc($result)){
                print_r($reg);
                echo "<br>";
            }
        }
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conexion);
    }
    // cerramos conexion
    mysqli_close($conexion)or die("Problemas para cerrar la sesion ".mysqli_error());



}

////////////////////////////////////////////
////////////////////////////////////////////



////////////////////////////////INICIO PROGRAMA///////////////////////////
//                      PART DESTINADA A LES VARIABLES INCIALS
//
$error=false;
$missatgerror['nomAlumne']="";

//
//////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////
//
//		SECCIÓ 1: per comprovar si el s'ha enviat el formulari, usem el botó enviar, que tb forma part del REQUEST
//

if(isset($_REQUEST['enviar'])){

    $resultatNom=validaCamp("nom d'alumne",$_REQUEST['nomAlumne']);
    if ( $resultatNom !=""){
        $error = true;
        $missatgerror["nomAlumne"] = $resultatNom;
    }
}
/////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////
//
//		SECCIÓ 2: procés del formulari, si s'ha enviat i no hi ha errades
//

if (isset($_REQUEST['enviar']) and !($error)){

    $fichero=htmlspecialchars($_SERVER["PHP_SELF"]);
    // una vez hechas las comprovaciones le pasamos por parametros en nombre del alumno a buscar
    mostrarSelect($_REQUEST['nomAlumne']);

    echo "<br>";
    echo "<a href='$fichero'>Torna a introduir un altre usuari</a>";
    echo "<br>";
    echo "<br>";
    echo "<a href='Ex25_PaginaPrincipal.html'>Volver la pagina principal...</a>";
}


/////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////
//
//		SECCIÓ 3: es mostra el formulari original
//
else {
    ?>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <h1>Buscador d'alumne</h1>
        <fieldset>
            <legend>Cerca</legend>
            <div>
                <label for="">Nom d'alumne: </label>
                <input type="text" name="nomAlumne"  placeholder="Introduce aqui el nombre del alumno:" />
                <?php
                if ($missatgerror["nomAlumne"]!=""){
                    echo "<span class='error'>".$missatgerror["nomAlumne"]."</span>";
                }
                ?>
                <br>
                <input type="submit" name="enviar" value="Enviar">

            </div>
        </fieldset>
    </form>
    <?php
}
///////////////////////////////////////////////////
?>
</body>
</html>