<?php

require_once("../Helpers/helper_Login.php");

if (isset($_POST['login'])and !($error)){

    require_once("../Model/login_model.php");
    $check = new login_model();
    $hashUser = $check->getUserHash($_POST['dni']);

    if (password_verify($_POST['passwd'],$hashUser)){
        session_start();
        $_SESSION['id'] = $check->getId($_POST['dni']);
        $_SESSION['nombre'] = $check->getName($_POST['dni']);
        session_write_close();
        header('Location: ../Views/init.php');
    }else{
        $missatgerror="El usuario o contraseña no son correctos";
        require_once("../Views/login.php");
    }

}else{
    require_once("../Views/login.php");
}

?>