<html>
<head>
    <meta charset="utf-8">
    <title>Inicio</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
        *{
            font-family: "Roboto", serif;
        }
        div{
            margin: auto;
            border: 5px solid cyan;
            width: 80%;
            text-align: center;
            padding: 20px;
        }
        a{
            margin: 5px;
        }
    </style>
</head>
<body>
<?php

session_start();
$_POST['id'] = $_SESSION['id'];
session_write_close();

if (isset($_POST['iban'])){

    require_once("../Model/init_model.php");
    $conexion = new init_model();
    $saldo = $conexion->getSaldo($_POST['iban']);

    session_start();
    $_SESSION['iban_origen'] = $_POST['iban'];
    $_SESSION['saldo']=$saldo;
    $_SESSION['lista']=$conexion->getMovimientos($_POST['iban']);
    session_write_close();
    ?>
    <div>
        <header>
            <h1>Saldo</h1>
        </header>
        <h3><?php echo $saldo ?> €</h3>
        <nav>
            <a href="../Views/transfer.php">Transferencias </a>
            <a href="../Views/query.php">Búsqueda </a>
            <a href="../Views/init.php">Inicio </a>
        </nav>
    </div>

    <?php


}elseif (isset($_POST['nuevaCuenta'])){

    require_once("../Model/init_model.php");
    $conexion = new init_model();
    $conexion->setNuevaCuenta($_POST['id']);
    sleep(1);
    header('Location:' . getenv('HTTP_REFERER'));

}

?>

</body>
</html>

